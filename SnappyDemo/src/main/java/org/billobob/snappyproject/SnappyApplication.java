package org.billobob.snappyproject;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.util.FileSystemUtils;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * What does @SpringBootApplication do?
 *

 @Configuration tags the class as a source of bean definitions for the application context.

 @EnableAutoConfiguration tells Spring Boot to start adding beans based on classpath settings,
 other beans, and various property settings.

 Normally you would add @EnableWebMvc for a Spring MVC app, but Spring Boot adds it
 automatically when it sees spring-webmvc on the classpath. This flags the application
 as a web application and activates key behaviors such as setting up a DispatcherServlet.

 @ComponentScan tells Spring to look for other components, configurations, and services
 in the the hello package, allowing it to find the FileUploadController.

 */
@SpringBootApplication
public class SnappyApplication {

	public static void main(String[] args) {
		/**
		 * Note the complete lack of xml--this is a pure java applcation using
		 * .run
		 */
		SpringApplication.run(SnappyApplication.class, args);
	}

	// Java8 lambda handles creating a CommandLineRunner

	@Bean
	CommandLineRunner init() {
		return (args) -> {
			FileSystemUtils.deleteRecursively(new File(SnappyUploadController.ROOT));

			Files.createDirectory(Paths.get(SnappyUploadController.ROOT));
		};
	}
}


