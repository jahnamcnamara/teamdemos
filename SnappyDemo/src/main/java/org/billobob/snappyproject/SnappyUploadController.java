package org.billobob.snappyproject;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.config.annotation.PathMatchConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.xerial.snappy.Snappy;
import org.xerial.snappy.SnappyInputStream;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Collectors;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;


@Controller
public class SnappyUploadController {

	private static final Logger log = LoggerFactory.getLogger(SnappyUploadController.class);

	public static final String ROOT = "upload-dir";

	private final ResourceLoader resourceLoader;

	@Autowired
	public SnappyUploadController(ResourceLoader resourceLoader) {
		this.resourceLoader = resourceLoader;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/")
	public String provideUploadInfo(Model model) throws IOException {

		model.addAttribute("files", Files.walk(Paths.get(ROOT))
				.filter(path -> !path.equals(Paths.get(ROOT)))
				.map(path -> Paths.get(ROOT).relativize(path))
				.map(path -> linkTo(methodOn(SnappyUploadController.class).getFile(path.toString())).withRel(path.toString()))
				.collect(Collectors.toList()));

		return "uploadForm";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{filename:.+}", params={"link"})
	@ResponseBody
	public ResponseEntity<?> getFile(@PathVariable String filename) {

		try {
			return ResponseEntity.ok(resourceLoader.getResource("file:" + Paths.get(ROOT, filename).toString()));
		} catch (Exception e) {
			return ResponseEntity.notFound().build();
		}
	}

	@RequestMapping(method = RequestMethod.POST, value = "/")
	public String handleFileUpload(@RequestParam("file") MultipartFile file,
								   RedirectAttributes redirectAttributes) {

		if (!file.isEmpty()) {
			try {
				final long snappyStart = System.currentTimeMillis();
				final long origSize = file.getSize();
				final long snapSize = Files.copy(new ByteArrayInputStream(Snappy.compress(file.getBytes())),
						Paths.get(ROOT, file.getOriginalFilename()));
				final long snappyEnd = System.currentTimeMillis();
				redirectAttributes.addFlashAttribute("message",
						"You successfully uploaded " + file.getOriginalFilename() + " in "
				+ (snappyEnd - snappyStart) + " milliseconds! Original size: " + origSize + " bytes, snapped size: "
				+ snapSize);
			} catch (IOException|RuntimeException e) {
				redirectAttributes.addFlashAttribute("message", "Failued to upload " + file.getOriginalFilename() + " => " + e.getMessage());
			}
		} else {
			redirectAttributes.addFlashAttribute("message", "Failed to upload " + file.getOriginalFilename() + " because it was empty");
		}

		return "redirect:/";
	}


	@RequestMapping(value = "/{file_name}", method = RequestMethod.GET)
	public void getFile(
			@PathVariable("file_name") String fileName,
			HttpServletResponse response) {
		InputStream fis = null;
		log.info(ROOT + "/" + fileName);
		try {
			// get your file as InputStream
			fileName = Paths.get(ROOT, fileName).toString();
			fis = new FileInputStream(fileName);

		} catch (IOException ex) {
			log.info("Error loading file to inputstream. Filename was '{}'", fileName, ex);
			throw new RuntimeException("Error loading file to inputstream");
		}
		try {
			// copy it to response's OutputStream
			IOUtils.copy(new SnappyInputStream(fis), response.getOutputStream());
			response.flushBuffer();
		}
		catch (IOException ex) {
			log.info("Error decompressing with snappy '{}'", fileName, ex);
			throw new RuntimeException("IOError with snappyinputstream");
		}
	}

	// To allow mapping without ending regex to work
	@Configuration
	protected static class AllResources extends WebMvcConfigurerAdapter {

		@Override
		public void configurePathMatch(PathMatchConfigurer matcher) {
			matcher.setUseRegisteredSuffixPatternMatch(true);
		}

	}
}
